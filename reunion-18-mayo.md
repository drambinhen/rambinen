1. finales de ciclo - fanzines y libretas y programaciones por venir
2. cineforo pirata
3. proyecciones especiales
4. rr.ss

# Tareas
1. Julian averigua los discos duros externos para comprar
2. Planear reunión con Sick to Ill para conversar sobre las asperezas
3. 27 de mayo cada uno presenta una idea de qué se puede realizar en antimateria
4. Revisar películas y correo de cioneforo pirata para proponer calendario alternativo para el martes 21 de mayo 

## 1. Finales de ciclo
1. Fanzines: Hacer un fanzine con los textos del ciclo -uno por cada ciclo
2. Canchimalos: Proyección especial de "carta a mi otro yo" y "vienen las grietas" junto a un corto de danza al azar
3. Locación Secreta: 24 de mayo se proyecta igual, hacer proyección de cierre/inicio con la película de "It is night in America" junto a Cineforo Pirata y luego seleccionadores de música

### libretas
Se harán las libretas cocidas por nosotros, meterle coquetería de verdad

### Programaciones por venir
Está antimateria disponible para que pensemos un ciclo - 27 de mayo cada uno presenta una idea de qué se puede realizar en el lugar

## 2. Cineforo Pirata

## 3. Proyecciones especiales
- Malo pa' pintar en muñecos - Canchimalos
- Cine al azar 15 de junio - Santa Elena
- Mambo Cool con Máscara - Locación Secreta
- Descubriendo al autor con Simón Vélez - ¿?

## 4. RRSS
Tener más impacto en las redes pero que sea un trabajo conjunto con el espacio de proyección

#### Tinto de Verano
Realizar una reseña sobre cada director para ser publicada al iniciar la semana
