## Alexander Sokurov (14 de junio, 1951)

*"Solo soy un vínculo en la cadena del mundo cultural; y si eso no es así, entonces todo mi trabajo es basura. De hecho, me esfuerzo por encontrar vínculos con la tradición en cada una de mis piezas. Por eso no me llamen vanguardista. Los vanguardistas se esfuerzan por crear algo nuevo, comenzando por ellos mismos. El llamado a una cierta conexión ininterrumpida es quizás el único elemento intelectual en mi trabajo, y todo lo demás viene de la emoción"*

Cineasta ruso que nace en una familia militar ubicada en Siberia. En 1974 se gradúa del departamento de Historia de la Universidad Nizhny Novgorod y al siguiente año entra a los estudios VGIK. Estando dentro de este estudio se vuelve amigo de **Tarkovsky** del cual se influencia profundamente por su película *El Espejo*. La mayoría de sus primeras producciones fueron vetadas por las autoridades soviéticas.

### Creación 
Al pasar de los años, Sokurov ha realizado documentales, ficciones y videos, ha trabajado en producciones radiales y creado trabajos literarios. 
Aunque ha realizado casi cuarenta películas durante un periodo de dos décadas, todavía se encuentra en la búsqueda del lenguaje cinematográfico, el cual -como lo dice él- el ahora niño de cien años de edad -el Cine- todavía no ha sido capaz de desarrollar.

Sokurov puede ser impredecible, pero siendo siempre fiel a su acercamiento personal a las cosas. Las características distintivas de su trabajo incluye: el tiempo extendido de los acentos semánticos, métodos elaborados de filmar y procesar, la combinación del documental y la ficción, una combinación de sets construidos y locaciones naturales externas, y el empleo tanto de actores profesionales como amateurs.

La representación del mundo en sus películas está abierta a cualquier detalle imprevisto de la realidad objetiva, mientras se presenta a la vez como una composición abstracta en sí misma, una metáfora artística. Las secuencias en los trabajos documentales de Sokurov no sirven como una ilustración o interpretación de cierto concepto o idea, sino como una revelación del inesperado estado emocional de quien la observa.

Sokurov utiliza las nuevas tecnologías del cine como medios evocativos, sin subyugarse a las normas de apliación convencional de las mismas, al contrario, él subyuga estas posibilidades técnicas a sus intenciones artísticas, las actualiza constantemente para que sobrepasen los métodos contemporáneos de producción.

### Actividad política
En diciembre de 2016, durante un encuentro del Consulado de la Cultura y las Artes, Sokurov le pidió al presidente Vladimir Putin reconsiderar el veredicto en contra del cineasta Oleg Sentsov, el cual Putin rechazo.
(Oleg Sentsov fue arrestado en mayo del 2014 por sospecha de "planear actos terroristas")

Luego, en 2022, Sokurov criticó al Kremlin y se opuso a la guerra en Ucrania. Por eso, en junio del 2022, se le negó el derecho a dejar Rusia. Sokurov fue detenido al intentar cruzar en carro hacia Finlandia para luego viajar a Milan para asistir a una conferencia de arte. Al acercacerse a la frontera las autoridades rusas le confiscaron su pasaporte.

# Post
*"Solo soy un vínculo en la cadena del mundo cultural"*

Cineasta ruso que en 1975 entra a los estudios VGIK, donde se vuelve amigo de **Tarkovsky**, quien lo influencia profundamente con su película *El espejo*.
Aunque ha realizado más de cuarenta películas, continúa en la búsqueda del lenguaje cinematográfico, el cual dice, es un niño de cien años de edad que todavía no ha sido capaz de desarrollar.
Algo característico de Sokurov es la implementación de nuevas tecnologías como medios evocativos, sin subyugarse a las normas de aplicación convencional de las mismas.

